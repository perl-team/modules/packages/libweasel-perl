libweasel-perl (0.31-1) unstable; urgency=medium

  * Team upload.

  [ Debian Janitor ]
  * Update standards version to 4.6.2, no changes needed.

  [ Étienne Mollier ]
  * Import upstream version 0.31.
  * d/copyright: update copyright year.

 -- Étienne Mollier <emollier@debian.org>  Mon, 21 Aug 2023 22:38:16 +0200

libweasel-perl (0.29-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 0.29.
  * Update years of upstream copyright.
  * Declare compliance with Debian Policy 4.6.0.

 -- gregor herrmann <gregoa@debian.org>  Thu, 30 Sep 2021 18:21:03 +0200

libweasel-perl (0.27-1) unstable; urgency=medium

  * Team upload.

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Submit.
  * Update standards version to 4.5.0, no changes needed.

  [ gregor herrmann ]
  * Import upstream version 0.27.
  * Update years of upstream copyright.
  * Declare compliance with Debian Policy 4.5.1.
  * Set Rules-Requires-Root: no.
  * Bump debhelper-compat to 13.
  * Add new test and runtime dependency.

 -- gregor herrmann <gregoa@debian.org>  Sun, 10 Jan 2021 14:20:43 +0100

libweasel-perl (0.26-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 0.26.
  * Annotate test-only build dependencies with <!nocheck>.
  * Declare compliance with Debian Policy 4.4.1.
  * Bump debhelper-compat to 12.
  * debian/watch: use uscan version 4.
  * Remove obsolete fields Name, Contact from debian/upstream/metadata.

 -- gregor herrmann <gregoa@debian.org>  Sun, 10 Nov 2019 19:16:39 +0100

libweasel-perl (0.20-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 0.20.
  * Bump debhelper compatibility level to 11.

 -- gregor herrmann <gregoa@debian.org>  Thu, 21 Feb 2019 21:29:24 +0100

libweasel-perl (0.18-1) unstable; urgency=medium

  [ Robert James Clay ]
  * Import upstream version 0.18.
  * Declare compliance with Debian Policy 4.3.0.
  * Add 'libhtml-selector-xpath-perl' to package Depends as well as to the
    Build-Depends-Indep stanzas in debian/control.

  [ gregor herrmann ]
  * Update debian/upstream/metadata.
  * Update years of upstream copyright.

 -- Robert James Clay <jame@rocasa.us>  Tue, 12 Feb 2019 14:13:26 -0500

libweasel-perl (0.15-1) unstable; urgency=medium

  * Team upload.

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * Import upstream version 0.15
  * Update debian/upstream/metadata.
  * debian/control: update Homepage.
  * Drop patches, merged upstream.
  * Update years of upstream copyright.
  * Update (build) dependencies.
  * Declare compliance with Debian Policy 4.2.1.
  * Bump debhelper compatibility level to 10.

 -- gregor herrmann <gregoa@debian.org>  Sat, 20 Oct 2018 18:40:55 +0200

libweasel-perl (0.11-1) unstable; urgency=low

  * Initial Release. (Closes: #869624)

 -- Robert James Clay <jame@rocasa.us>  Wed, 04 Oct 2017 23:29:05 -0400
